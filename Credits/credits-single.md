---
author: 
  - 'オーギュスト・エスコフィエ'
  - '五 島　学　訳・注'
title: 'フランス料理の手引き'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frsubenv:
  - frsubenv
  recette:
  - recette
  multicols:
  - multicols
  nota:
  - nota
---

\thispagestyle{empty}

##### 訳者略歴 {#petits-cv}

**五島　学**

1966 年東京生まれ。東京都立大学大学院人文科学研究科博士課程単位取得満期
退学（仏文学専攻）。パリ第 XII 大学 Val-de-Marne 校 DEA 取得。宇都宮大学、
明治大学等非常勤講師を経て 2005 年より在野でフランス食文化史研究に取り組
みつつ、レストラン向けにヨーロッパ品種の野菜を生産。2011年〜2014年、柴
田書店「月刊 専門料理」連載「エスコフィエを読む」において訳と注釈を担
当。共著『エスコフィエの新解釈』旭屋出版、2018年。

\vfil

\vfil

\vfil

\vfil

\vfil

\vfil


<div class="flushright">

\LARGE



[エスコフィエ　フランス料理の手引き]{.credittitle}



</div><!--endFlushright-->

<div class="flushright">

\large 2022 年 12 月 31 日初版発行

\vspace{2zw}

\vspace{2ex}

\normalsize

\Large 

[訳・注……五島　学]{.creditauthor}




\normalsize

下訳、校閲、校正……江畑　雄一、河井　健司、五島　充代

善搭　一幸、春野　裕征、山下　拓也

デザイン、組版……レ プゥス ヴェール

発行者……五島　学

</div><!--endFlushright-->

<div class="flushright">

\Large

[発行所……レ プゥス ヴェール]{.creditauthor}

\normalsize

〒370-3405 群馬県高崎市倉渕町川浦 2786

電話……027-360-9030

http://lespoucesverts.org

\Large [印刷・製本……□□□□□□]{.creditauthor}

</div><!--endFlushright-->

\setstretch{0.8}

© 2021 五島 学

無断転載、翻訳、複写、データ配信等を禁ず。

ISBN ○○○-○-○○○-○○○○○-○
