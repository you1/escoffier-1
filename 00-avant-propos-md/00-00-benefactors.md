---
author: '五 島　学　訳・注'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ　フランス料理の手引き'
---

## 謝 辞 {#acknowledgments epub:type=acknowledgments}

本書の翻訳および注釈の作成にあたって、多くの方々のお世話になった。なかでもとりわ
け、河井健司氏（田園調布、Un de ces Jours オーナーシェフ）および善搭一幸氏（阿佐ヶ
谷、ラ メゾン クルティーヌ、オーナーシェフ）には資金、下訳、料理技術的助言など全
面的にご尽力いただいた。また、山本聖司氏（神楽坂、La Tourelle オーナーシェフ）に
はもっぱら資金面と料理業界情報などで支えてくださった。畏友ともいうべきお三方にま
ず心からの感謝を申しあげる。

本書の日本語訳は現行版である第四版（フラマリオン社刊）を底本とし、随時初版〜第三
版を参照した。もとより原書で本文 900 ページの大著、それも百年前の本を一語一句あ
ますところなく現代日本語に訳し、さらにその食文化史的背景はもとより、当時の社会情
勢、フランスという地域の文化一般にまで及ぶ注釈をつける作業が、時間と費用を要する
ものであることはいうまでもないだろう。その点で、下記の皆様からの善意ある資金提供
により執筆を支えていただけたことは大きい。ここにご芳名を記すことで謝意を表したく
思う（ご支援順、敬称略）。

\normalsize
\vspace{1ex}

\noindent
Sin Masui、<!--20180524-23h,2x,novelsoundsmail@gmail.com-->
[**河井健司** (Un de ces Jours)](http://www.undecesjours.com/)、<!--20180525-0h14,10x,20180605-11h42,10x,20120212,10x,kwibeng@gmail.com-->
Yuya HASHIMOTO、<!--20180525-1h40,2x,hashimo0910@gmail.com-->
[**善塔一幸** (La Maison Courtine)](http://www.courtine.jp/)、<!--20180525-8h56,10x,20190212,2x,kazuyukizento120@docomo.ne.jp-->
藤井智大 (フランス料理店 ミルエテ)、<!--[](20180525-10h07,1x,apple19761019@yahoo.co.jp)-->
kojima mio、<!--[](20180525-12h23,1x,teeeeshow@yahoo.co.jp)-->
[**春野裕征** (La Hönnêtêté)](https://www.facebook.com/lahonnetete/)、<!--[](20180528-2h41,20180617,20180818,20180919,4x,20190702,1xamanojack.v-o-v@i.softbank.jp)-->
[**大谷岳史**（Les Chevreuils）](https://leschevreuils.jimdo.com/)、<!--[](20180529-17h26,2x,20190212-10h51,x5,1000feuille@ezweb.ne.jp)--> <!--20190701,5x-->
澤井隆太、<!--[](20180530-2h57.1x,20190212,0.2x,rs.ajtk.zz@i.softbank.jp)-->
**Takuya Yamashita** (Whitegrass, Singapore)、<!--[](20180605-11h04,5x,20120212,x6,FB:Takuya Yamashita, 20190725x10,20191012x3)-->
UNNO GENKI、<!--[](20180605-22h12,1x,guriiva@aol.com)-->
**石幡乾**、<!--[](201806060-6h24,1x,20190212,0.6x,20200117x10zonek.gentile.bonheur.ken05@gmail.com)-->
**山尾圭輔**、<!--[](20180612-10h27,20x,yamaokeisuke@hotmail.com)-->
手島純也、<!--[](20180612-19h19,1x,junchef1975@yahoo.co.jp)-->
[**山本聖司** (La Tourelle)](http://www.tourelle.jp/)、<!--[](20180622,10x,20190212,1,6x,tourelle@tourelle.jp)-->
樋口直哉、<!--[](20180622-10h58,1x)-->
OYAMA YOSHITAKA、<!--[](20180622-22h50,x2) -->
<!--高橋 昇(レストラン ドゥ ラパン)、[](20180710-2h16,1x)-->
山本学 (レチュード)、<!--[](20180711, 現物6口相当)-->
磯崎伊佐男、<!--[](20180718, 4x, w07jbii@softbank.ne.jp)-->
神山裕之、<!--[](20180831, 1x, moltke01@gmail.com )-->
稲葉晋一 (ノースタイラー)、<!--[](20190104, 3x, nt-d-rx-o@ezweb.ne.jp)-->
乾裕一、<!--[](20190105, x1, hirokazu.86-9-4@docomo.ne.jp)-->
江畑雄一、
有馬基、<!--20190212,x2-->
笠上勇一、<!--20190212,x1.5.6-->
川添祐輝、<!--20190212, x1-->
栗原知彦、<!--30190212,0x1-->
五木田祐人、<!--20120212,1x-->
更井亮介、<!--20190310,2x-->
大澤祐介、<!--20190311,2x-->
北川拓朗、<!--20190504,1x-->
外山和己、<!--20190611, 2x-->
<!--ogisu shunsuke、匿名 201907701,2x)-->
田中伸幸（田中医院）、<!--20190703, 2x-->
**KEIICHI KAMOSHITA**、<!--20190726,11x-->
八木尚子、 <!--30190817,x2-->
藤岡俊平、 <!--20191206,x2-->
土肥秀幸、<!--20191217,x1-->
[岡島卓巳（株式会社辻料理教育研究所）]{.tsuji}、<!--20191217,x2-->
石井ＡＱ、<!--20191218,x2-->
矢吹努、<!--20200218,x2-->
辻井一真、<!--20200411,x1-->

\vfill


このほか、フェイスブックのグループ「エスコフィエ『料理の手引き』全注解プロジェク
ト」に参加してくださった諸氏には訳語や解釈、技法についてのアンケートなどにご協力
いただき、ほかにも大いに示唆をいただけたことはまことにありがたく思う。また、実際
の作業（下訳、校正など）に協力してくださった江畑雄一氏、春野裕征氏、山下拓也氏に
も心からお礼申しあげたい。

最後になってしまったが、うつを患っている編著者の日常を支え、下訳の一部を担ってく
れた妻、｜充代《みちよ》に感謝を述べたい。また、<!--（株）旭屋出版の永瀬正人
氏、井上久尚氏、オフィス・スノウの畑中三応子氏、木村奈緒氏にも心より厚くお礼申し
あげるとともに、-->本書を手にとってくださり、日本のフランス料理、ガストロノミー
の将来と真剣に向きあっておられる読者たる料理人諸氏および教養として食文化史を考え
てくださる読者諸賢にお礼申しあげる。

<div class="flushright">2021年12月 訳者しるす</div>
<!--20190214江畑校正確認スミ-->
